package audit

import (
	"encoding/binary"
	"math/rand"
	"time"
)

func init() {
	// Just need to differentiate between runs, not aiming for
	// cryptographic safety here.
	rand.Seed(time.Now().Unix())
}

// Generate a new time-based unique ID for use as primary key. The
// resulting IDs can be sorted lexicographically preserving the time
// ordering.
func NewUniqueId(t time.Time) []byte {
	var b [16]byte

	// The key consists of serialized time + sufficiently large
	// random number. This should allow a high insertion rate, and
	// hopefully prevent conflicts considering the rand global lock.
	binary.BigEndian.PutUint64(b[:8], uint64(t.UnixNano()))
	binary.BigEndian.PutUint64(b[8:16], uint64(rand.Int63()))

	return b[:]
}
