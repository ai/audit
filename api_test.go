package audit

import (
	"testing"
)

var okTestData = `
{
  "timestamp": "2009-11-10T23:00:00Z",
  "user": "test",
  "message": "sample message"
}
`

func TestMessage_FromJSON(t *testing.T) {
	m, err := MessageFromJSON([]byte(okTestData))
	if err != nil {
		t.Fatal(err)
	}
	ts := m.Stamp()
	if ts.Unix() == 0 {
		t.Fatal("timestamp is null")
	}
}

func TestMessage_IsValid(t *testing.T) {
	m, err := MessageFromJSON([]byte(okTestData))
	if err != nil {
		t.Fatal(err)
	}
	if !m.IsValid() {
		t.Fatal("not valid")
	}
}

func TestMessage_IsNotValid(t *testing.T) {
	m, err := MessageFromJSON([]byte(`
{
  "user": "test",
  "message": "some message"
}
`))
	if err != nil {
		t.Fatal(err)
	}
	if m.IsValid() {
		t.Fatal("valid")
	}
}
