package main

import (
	"flag"
	"fmt"
	"log"

	"git.autistici.org/ai/audit/server"
)

var (
	sslCa   = flag.String("ssl-ca", "/etc/ai/internal_ca.pem", "SSL CA file")
	sslCert = flag.String("ssl-cert", "/etc/ai/localhost_internal.pem", "SSL certificate file")
	sslKey  = flag.String("ssl-key", "/etc/ai/localhost_internal.key", "SSL private key file")
	dbDir   = flag.String("data-dir", "/var/lib/auditd", "Path to the database directory")
	port    = flag.Int("port", 1717, "TCP port to listen on")
)

func main() {
	flag.Parse()

	db := server.NewDB(*dbDir, nil)
	server := server.NewHttpServer(db)
	if err := server.ListenAndServeTLS(fmt.Sprintf(":%d", *port), *sslCa, *sslCert, *sslKey); err != nil {
		log.Fatal(err)
	}
}
