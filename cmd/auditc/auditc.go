// Command-line client for the audit log collection suite.
package main

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"flag"
	"log"
	"os"
	"net/http"
	"strings"

	"git.autistici.org/ai/audit"
)

var (
	sslCa     = flag.String("ssl-ca", "/etc/ai/internal_ca.pem", "SSL CA file")
	sslCert   = flag.String("ssl-cert", "/etc/ai/localhost_internal.pem", "SSL certificate file")
	sslKey    = flag.String("ssl-key", "/etc/ai/localhost_internal.key", "SSL private key file")
	spoolDir  = flag.String("spool-dir", "/var/spool/audit/incoming", "Path to the spool directory")
	serverUrl = flag.String("server", "https://audit.x.investici.org:1717", "URL for the main audit server")
)

func parseQuery(args []string) map[string]string {
	query := make(map[string]string)
	for _, arg := range args {
		if !strings.Contains(arg, "=") {
			log.Fatal("Could not parse key=value argument: '%s'", arg)
		}
		parts := strings.SplitN(arg, "=", 2)
		query[parts[0]] = parts[1]
	}
	return query
}

func runQuery(url string, tlsConf *tls.Config, query map[string]string) []audit.Message {
	httpClient := http.Client{
		Transport: &http.Transport{
			TLSClientConfig: tlsConf,
		},
	}

	// Encode the query to JSON.
	var b bytes.Buffer
	json.NewEncoder(&b).Encode(query)
	resp, err := httpClient.Post(url, "application/json", &b)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		log.Fatalf("Bad HTTP response: %s", resp.Status)
	}

	// Decode the query.
	var result struct {
		Results []audit.Message `json:"results"`
	}
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		log.Fatalf("Error decoding response: %s", err)
	}

	return result.Results
}

func main() {
	flag.Parse()

	query := parseQuery(flag.Args())
	if len(query) == 0 {
		log.Fatal("No query specified")
	}

	tlsConf := audit.TLSClientAuthConfigWithCerts(*sslCa, *sslCert, *sslKey)
	results := runQuery(strings.TrimRight(*serverUrl, "/")+"/api/1/query", tlsConf, query)
	for _, msg := range results {
		json.NewEncoder(os.Stdout).Encode(msg)
		os.Stdout.Write([]byte{'\n'})
	}
}
