package main

import (
	"flag"
	"log"
	"os"

	"git.autistici.org/ai/audit/server"
)

var (
	doDump = flag.Bool("dump", false, "dump all messages in the db")
	doRestore = flag.Bool("restore", false, "restore messages from a dump")
	dbDir = flag.String("data-dir", "/var/lib/auditd", "Path to the database directory")
)

func main() {
	log.SetFlags(0)
	flag.Parse()

	if (!*doDump && !*doRestore) || (*doDump && *doRestore) {
		log.Fatal("Must specify either --dump or --restore")
	}

	db := server.NewDB(*dbDir, nil)
	defer db.Close()

	var err error
	if *doDump {
		err = db.Dump(os.Stdout)
	} else {
		err = db.Restore(os.Stdin)
	}
	if err != nil {
		log.Fatal(err)
	}
}
