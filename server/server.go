package server

import (
	"encoding/json"
	"log"
	"net"
	"net/http"
	"strings"
	"time"

	"git.autistici.org/ai/audit"
)

type HttpServer struct {
	database *DB
}

func NewHttpServer(db *DB) *HttpServer {
	return &HttpServer{database: db}
}

func remoteHost(r *http.Request) string {
	if host, _, err := net.SplitHostPort(r.RemoteAddr); err != nil {
		return host
	}
	return r.RemoteAddr
}

func dumpMsg(msg audit.Message) string {
	return strings.TrimRight(string(msg.ToJSON()), "\n")
}

// Write handler: the input data is already a JSON message.
func (h *HttpServer) writeHandler(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Bad Request Type", http.StatusBadRequest)
		return
	}

	msg, err := audit.MessageFromJSONReader(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Log all received messages, even if there is an error later.
	log.Printf("%s: received message: %s", remoteHost(r), dumpMsg(msg))

	if !msg.IsValid() {
		log.Printf("%s: invalid message", remoteHost(r))
		http.Error(w, "Invalid Message", http.StatusBadRequest)
		return
	}

	if err := h.database.Write(msg); err != nil {
		log.Printf("%s: error writing to database: %v", remoteHost(r), err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Request was successful, return an empty 200 OK reply.
	w.WriteHeader(200)
}

// Query handler. Accept query as a free-form JSON dictionary.
func (h *HttpServer) queryHandler(w http.ResponseWriter, r *http.Request) {
	var query map[string]string
	if err := json.NewDecoder(r.Body).Decode(&query); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	results, err := h.database.Query(query)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Serve the results wrapped in a JSON dictionary.
	response := struct {
		Results []audit.Message `json:"results"`
	}{Results: results}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// Start the server (with TLS).
func (h *HttpServer) ListenAndServeTLS(addr, caFile, certFile, keyFile string) error {
	mux := http.NewServeMux()
	mux.HandleFunc("/api/1/write", h.writeHandler)
	mux.HandleFunc("/api/1/query", h.queryHandler)

	server := &http.Server{
		Addr:         addr,
		Handler:      mux,
		ReadTimeout:  time.Duration(10 * time.Second),
		WriteTimeout: time.Duration(60 * time.Second),
		TLSConfig:    audit.TLSClientAuthConfig(caFile),
	}
	return server.ListenAndServeTLS(certFile, keyFile)
}
