package server

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"git.autistici.org/ai/audit"
)

func TestDB_Insert(t *testing.T) {
	dbdir, _ := ioutil.TempDir("", "auditdb-")
	defer os.RemoveAll(dbdir)

	db := NewDB(dbdir, nil)
	defer db.Close()

	msg, _ := audit.MessageFromJSON([]byte(`{"user": "a", "message": "cheers", "stamp": 123456789}`))
	if err := db.Write(msg); err != nil {
		t.Fatal(err)
	}
}

func testInsertRecords(db *DB, n int) error {
	for i := 0; i < n; i++ {
		mstr := fmt.Sprintf(`{"user": "user%d", "host": "one", "message": "yow", "stamp": %d}`, i, 123456789+i)
		msg, _ := audit.MessageFromJSON([]byte(mstr))
		if err := db.Write(msg); err != nil {
			return err
		}
	}
	return nil
}

func TestDB_QueryRealData(t *testing.T) {
	dbdir, _ := ioutil.TempDir("", "auditdb-")
	defer os.RemoveAll(dbdir)

	db := NewDB(dbdir, nil)
	defer db.Close()

	testData := []string{
		`{"host":"opposizione","message":"created email solemnis@privacyrequired.com","request_type":"email","source":"accountserver","stamp":1.437922949e+09,"user":"solemnis@privacyrequired.com","who":"ai-services"}`,
		`{"alias":"info1@grrlz.net","host":"opposizione","message":"deleted alias info1@grrlz.net","source":"accountserver","stamp":1.434558486e+09,"user":"elitee@hacari.com","who":"elitee@hacari.com"}`,
		`{"host":"opposizione","message":"password reset","source":"accountserver","stamp":1.439408672e+09,"user":"blackwolf@autistici.org","who":"accountserver"}`,
		`{"host":"opposizione","message":"password reset","source":"accountserver","stamp":1.432290377e+09,"user":"blackwolf@autistici.org","who":"accountserver"}`,
		`{"alias":"skankindaddy@autistici.org","host":"opposizione","message":"created alias skankindaddy@autistici.org","source":"accountserver","stamp":1.444286071e+09,"user":"ilnonno@autistici.org","who":"ilnonno@autistici.org"}`,
		`{"alias":"sknkindaddy@autistici.org","host":"opposizione","message":"deleted alias sknkindaddy@autistici.org","source":"accountserver","stamp":1.44428608e+09,"user":"ilnonno@autistici.org","who":"ilnonno@autistici.org"}`,
		`{"host":"opposizione","message":"password reset","source":"accountserver","stamp":1.426104104e+09,"user":"blackwolf@autistici.org","who":"accountserver"}`,
		`{"host":"opposizione","message":"password reset","source":"accountserver","stamp":1.423795607e+09,"user":"blackwolf@autistici.org","who":"accountserver"}`,
		`{"host":"opposizione","message":"password reset","source":"accountserver","stamp":1.445412207e+09,"user":"blackwolf@autistici.org","who":"accountserver"}`,
	}
	for _, data := range testData {
		msg, _ := audit.MessageFromJSON([]byte(data))
		if err := db.Write(msg); err != nil {
			t.Fatal(err)
		}
	}

	// Test simple query.
	result, err := db.Query(map[string]string{"user": "blackwolf@autistici.org"})
	if err != nil {
		t.Fatal(err)
	}
	if len(result) != 5 {
		t.Fatalf("bad # of results: %+v", result)
	}
	if result[0]["user"].(string) != "blackwolf@autistici.org" {
		t.Fatalf("bad result: %+v", result[0])
	}
}

func TestDB_Query(t *testing.T) {
	dbdir, _ := ioutil.TempDir("", "auditdb-")
	defer os.RemoveAll(dbdir)

	db := NewDB(dbdir, nil)
	defer db.Close()

	testData := []string{
		`{"user": "a", "host": "one", "message": "wow", "stamp": 123456789}`,
		`{"user": "b", "host": "one", "message": "wow", "stamp": 123456790}`,
		`{"user": "c", "host": "two", "message": "wow", "stamp": 123456790}`,
	}
	for _, data := range testData {
		msg, _ := audit.MessageFromJSON([]byte(data))
		if err := db.Write(msg); err != nil {
			t.Fatal(err)
		}
	}

	// Test simple query.
	result, err := db.Query(map[string]string{"user": "a"})
	if err != nil {
		t.Fatal(err)
	}
	if len(result) != 1 {
		t.Fatalf("bad # of results: %+v", result)
	}
	if result[0]["user"].(string) != "a" {
		t.Fatalf("bad result: %+v", result[0])
	}

	// Test query with two expected results.
	result, err = db.Query(map[string]string{"host": "one"})
	if err != nil {
		t.Fatal(err)
	}
	if len(result) != 2 {
		t.Fatalf("bad # of results: %+v", result)
	}

	// Test composed query.
	result, err = db.Query(map[string]string{"user": "a", "host": "one"})
	if err != nil {
		t.Fatal(err)
	}
	if len(result) != 1 {
		t.Fatalf("bad # of results: %+v", result)
	}
	if result[0]["user"].(string) != "a" {
		t.Fatalf("bad result: %+v", result[0])
	}
}

func TestDB_DumpAndRestore(t *testing.T) {
	dbdir, _ := ioutil.TempDir("", "auditdb-")
	defer os.RemoveAll(dbdir)

	db := NewDB(dbdir, nil)
	defer db.Close()

	testData := []string{
		`{"user": "a", "host": "one", "message": "wow", "stamp": 123456789}`,
		`{"user": "b", "host": "one", "message": "wow", "stamp": 123456790}`,
		`{"user": "c", "host": "two", "message": "wow", "stamp": 123456790}`,
	}
	for _, data := range testData {
		msg, _ := audit.MessageFromJSON([]byte(data))
		if err := db.Write(msg); err != nil {
			t.Fatal(err)
		}
	}

	var buf bytes.Buffer
	if err := db.Dump(&buf); err != nil {
		t.Fatalf("Dump: %v", err)
	}

	dbdir2, _ := ioutil.TempDir("", "auditdb-")
	defer os.RemoveAll(dbdir2)
	db2 := NewDB(dbdir2, nil)
	defer db2.Close()
	if err := db2.Restore(&buf); err != nil {
		t.Fatalf("Restore: %v", err)
	}
}

// Benchmark straight insertion performance.
func BenchmarkDB_Insert(b *testing.B) {
	dbdir, _ := ioutil.TempDir("", "auditdb-")
	defer os.RemoveAll(dbdir)

	db := NewDB(dbdir, nil)
	defer db.Close()

	b.ResetTimer()
	if err := testInsertRecords(db, b.N); err != nil {
		b.Fatal(err)
	}
}

// Benchmark query performance with simple query.
func BenchmarkDB_QuerySimple(b *testing.B) {
	dbdir, _ := ioutil.TempDir("", "auditdb-")
	defer os.RemoveAll(dbdir)

	db := NewDB(dbdir, nil)
	defer db.Close()

	if err := testInsertRecords(db, 100000); err != nil {
		b.Fatal(err)
	}
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, err := db.Query(map[string]string{"user": "user13"})
		if err != nil {
			b.Error(err)
		}
	}
}

// Benchmark query performance with large query.
func BenchmarkDB_QueryLarge(b *testing.B) {
	dbdir, _ := ioutil.TempDir("", "auditdb-")
	defer os.RemoveAll(dbdir)

	db := NewDB(dbdir, nil)
	defer db.Close()

	if err := testInsertRecords(db, 100000); err != nil {
		b.Fatal(err)
	}
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, err := db.Query(map[string]string{"host": "one", "user": "user13"})
		if err != nil {
			b.Error(err)
		}
	}
}
