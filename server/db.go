package server

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"strings"

	"git.autistici.org/ai/audit"
	"git.autistici.org/ai/audit/Godeps/_workspace/src/github.com/syndtr/goleveldb/leveldb"
	ldbfilter "git.autistici.org/ai/audit/Godeps/_workspace/src/github.com/syndtr/goleveldb/leveldb/filter"
	ldbopt "git.autistici.org/ai/audit/Godeps/_workspace/src/github.com/syndtr/goleveldb/leveldb/opt"
	ldbutil "git.autistici.org/ai/audit/Godeps/_workspace/src/github.com/syndtr/goleveldb/leveldb/util"
)

// DB is a very simple indexed database implementation using LevelDB.
// Records can't be deleted. Any string value in the incoming message
// is indexed, which makes it possible to run simple key=value
// queries.
//
// The database is organized as follows:
//
// We only support exact key=value matches, so the index is modeled
// directly on top of leveldb primitives (scanning key ranges), which
// makes insertions really fast.
//
// For every message, we generate a unique id, and we store it under
// messages/<id>. Then we extract all the indexable key/value pairs,
// and store the message id under index/<key>/<value>/<id> (yes, the
// id appears twice, it's part of the key just to ensure uniqueness).
//
// Keys and values can be anything as long as they don't contain a
// null byte (which is used internally as the primary key separator).
//
type DB struct {
	leveldb      *leveldb.DB
	excludedKeys map[string]struct{}
}

// Options for database creation.
type DBOptions struct {
	LRUCacheSize       int
	BloomFilterSizeExp int
	ExcludedKeys       []string
}

// Open a database at 'path' with the given options.
func NewDB(path string, dbopts *DBOptions) *DB {
	// Normalize options.
	if dbopts == nil {
		dbopts = &DBOptions{}
	}
	if dbopts.LRUCacheSize == 0 {
		dbopts.LRUCacheSize = 2 << 20
	}
	if dbopts.BloomFilterSizeExp == 0 {
		dbopts.BloomFilterSizeExp = 10
	}

	// Set LevelDB options and create the database.
	opts := &ldbopt.Options{
		BlockCacheCapacity: dbopts.LRUCacheSize,
		Filter:             ldbfilter.NewBloomFilter(dbopts.BloomFilterSizeExp),
	}

	ldb, err := leveldb.OpenFile(path, opts)
	if err != nil {
		log.Fatalf("Error opening database: %s", err)
	}

	db := &DB{
		leveldb:      ldb,
		excludedKeys: make(map[string]struct{}),
	}

	// Exclude some keys by default.
	db.excludedKeys["message"] = struct{}{}
	db.excludedKeys["stamp"] = struct{}{} // old field for timestamp
	db.excludedKeys["timestamp"] = struct{}{}
	for _, key := range dbopts.ExcludedKeys {
		db.excludedKeys[key] = struct{}{}
	}
	return db
}

// Close the database and release associated resources.
func (db *DB) Close() {
	db.leveldb.Close()
}

func (db *DB) isExcluded(key string) bool {
	_, ok := db.excludedKeys[key]
	return ok
}

var keySeparator = byte(0)

func makeKey(prefix string, parts ...[]byte) []byte {
	allParts := make([][]byte, 0, len(parts)+1)
	allParts = append(allParts, []byte(prefix))
	allParts = append(allParts, parts...)
	return bytes.Join(parts, []byte{keySeparator})
}

func makePrefixKeyRange(prefix []byte) ([]byte, []byte) {
	startKey := make([]byte, len(prefix)+1)
	copy(startKey, prefix)
	startKey[len(prefix)] = keySeparator
	endKey := make([]byte, len(prefix)+1)
	copy(endKey, prefix)
	endKey[len(prefix)] = keySeparator + 1
	return startKey, endKey
}

func (db *DB) writeToBatch(msg audit.Message, wb *leveldb.Batch) {
	// Generate a unique ID for the message.
	msgid := audit.NewUniqueId(msg.Stamp())
	wb.Put(makeKey("messages", msgid), msg.ToJSON())

	// Extract the key/value pairs to be indexed.
	for key, genericValue := range msg {
		// Only string keys are indexed.
		value, ok := genericValue.(string)
		if !ok {
			continue
		}
		if db.isExcluded(key) {
			continue
		}

		wb.Put(makeKey("index", []byte(key), []byte(value), msgid), msgid)
	}
}

// Write a Message to the database.
func (db *DB) Write(msg audit.Message) error {
	wb := new(leveldb.Batch)
	db.writeToBatch(msg, wb)
	return db.leveldb.Write(wb, nil)
}

// Dump contents to a writer.
func (db *DB) Dump(w io.Writer) error {
	startKey, endKey := makePrefixKeyRange(makeKey("messages"))
	i := db.leveldb.NewIterator(
		&ldbutil.Range{
			Start: startKey,
			Limit: endKey,
		},
		&ldbopt.ReadOptions{DontFillCache: true},
	)
	for i.Next() {
		fmt.Fprintf(w, "%s\n", i.Value())
	}
	i.Release()
	if err := i.Error(); err != nil {
		return err
	}
	return nil
}

// Restore contents from a dump.
func (db *DB) Restore(r io.Reader) error {
	wb := new(leveldb.Batch)

	errs := 0
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		msg, err := audit.MessageFromJSON(scanner.Bytes())
		if err != nil {
			errs++
			continue
		}
		db.writeToBatch(msg, wb)
	}
	if err := scanner.Err(); err != nil {
		return err
	}
	if errs > 0 {
		return fmt.Errorf("failed to restore %d records", errs)
	}

	return db.leveldb.Write(wb, nil)
}

type keySet map[string]struct{}

func (r keySet) Add(key []byte) {
	r[string(key)] = struct{}{}
}

func (r keySet) Intersect(other keySet) keySet {
	// Find the smaller set so that a < b.
	var a, b keySet
	if len(r) < len(other) {
		a = r
		b = other
	} else {
		a = other
		b = r
	}

	// Iterate on 'a'.
	out := make(keySet)
	for value, _ := range a {
		if _, ok := b[value]; ok {
			out[value] = struct{}{}
		}
	}

	return out
}

// Query a key/value index. This will load *all* matching keys
// in memory, which might take quite a lot of space.
func (db *DB) queryIndex(key, value string) (keySet, error) {
	startKey, endKey := makePrefixKeyRange(makeKey("index", []byte(key), []byte(value)))
	i := db.leveldb.NewIterator(
		&ldbutil.Range{
			Start: startKey,
			Limit: endKey,
		},
		&ldbopt.ReadOptions{DontFillCache: true},
	)

	keys := make(keySet)
	for i.Next() {
		keys.Add(i.Value())
	}
	i.Release()
	if err := i.Error(); err != nil {
		return nil, err
	}
	return keys, nil
}

// Retrieve a bunch of messages from the db.
func (db *DB) getMessages(set keySet) ([]audit.Message, error) {
	var lastErr error
	result := make([]audit.Message, 0, len(set))
	for key, _ := range set {
		data, err := db.leveldb.Get([]byte(key), nil)
		if err != nil {
			lastErr = err
			continue
		}
		msg, err := audit.MessageFromJSON(data)
		if err != nil {
			lastErr = err
			continue
		}
		result = append(result, msg)
	}

	return result, lastErr
}

type subqueryResult struct {
	set keySet
	err error
}

// Query the database and return the results.
func (db *DB) Query(q map[string]string) ([]audit.Message, error) {
	// Query indexes in parallel, aggregate their results on a
	// channel.
	resultCh := make(chan subqueryResult)
	defer close(resultCh)
	for key, value := range q {
		go func(key, value string) {
			set, err := db.queryIndex(key, value)
			resultCh <- subqueryResult{set: set, err: err}
		}(key, value)
	}

	// Read result sets from the channel and intersect them.
	errors := make([]string, 0)
	var curset keySet
	for i := 0; i < len(q); i++ {
		r := <-resultCh
		if r.err != nil {
			errors = append(errors, r.err.Error())
			continue
		}
		if curset == nil {
			curset = r.set
		} else {
			curset = curset.Intersect(r.set)
		}
	}
	if len(errors) > 0 {
		return nil, fmt.Errorf("query errors: %s", strings.Join(errors, "; "))
	}

	// Retrieve rows from the key set.
	return db.getMessages(curset)
}
