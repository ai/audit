package audit

import (
	"bytes"
	"encoding/json"
	"io"
	"time"
)

// A message is a map of strings to arbitrary types.
type Message map[string]interface{}

var mandatoryAttributes = []string{
	"user", "message", "timestamp",
}

func (m Message) IsValid() bool {
	for _, attr := range mandatoryAttributes {
		if _, ok := m[attr]; !ok {
			return false
		}
	}
	return true
}

func (m Message) GetString(key string) string {
	if value, ok := m[key]; ok {
		return value.(string)
	}
	return ""
}

func (m Message) Stamp() time.Time {
	t, _ := time.Parse(m.GetString("timestamp"), time.RFC3339)
	return t
}

func (m Message) ToJSON() []byte {
	var b bytes.Buffer
	json.NewEncoder(&b).Encode(m)
	return b.Bytes()
}

func MessageFromJSON(data []byte) (Message, error) {
	var m Message
	if err := json.NewDecoder(bytes.NewBuffer(data)).Decode(&m); err != nil {
		return nil, err
	}
	return m, nil
}

func MessageFromJSONReader(r io.Reader) (Message, error) {
	var m Message
	if err := json.NewDecoder(r).Decode(&m); err != nil {
		return nil, err
	}
	return m, nil
}
