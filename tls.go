package audit

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"log"
)

var cipherSuites = []uint16{
	tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
	tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
}

// Load a CA from file (PEM encoded).
func loadCA(caFile string) *x509.CertPool {
	data, err := ioutil.ReadFile(caFile)
	if err != nil {
		log.Fatal(err)
	}
	pool := x509.NewCertPool()
	if !pool.AppendCertsFromPEM(data) {
		log.Fatal("Could not load CA certificate")
	}
	return pool
}

// Create a tls.Config enforcing CA-based client authentication. The
// resulting Config can be used by clients and servers alike. It
// forces some strict connection parameters since we control both
// endpoints and don't have to worry about wide compatibility.
func TLSClientAuthConfig(caFile string) *tls.Config {
	return &tls.Config{
		ClientCAs:                loadCA(caFile),
		ClientAuth:               tls.RequireAndVerifyClientCert,
		MinVersion:               tls.VersionTLS12,
		CipherSuites:             cipherSuites,
		PreferServerCipherSuites: true,
	}
}

// Same as TLSClientAuthConfig, but load client certificates too.
func TLSClientAuthConfigWithCerts(caFile, certFile, keyFile string) *tls.Config {
	config := &tls.Config{
		RootCAs:      loadCA(caFile),
		MinVersion:   tls.VersionTLS12,
		CipherSuites: cipherSuites,
	}

	var err error
	config.Certificates = make([]tls.Certificate, 1)
	config.Certificates[0], err = tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		log.Fatalf("Could not load certs: %s", err)
	}

	return config
}
